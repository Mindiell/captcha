# encoding: utf-8
"""
This module is used in order to create audio challenges
"""

import glob
import json
import os
import random
import re
import subprocess
import time

import click
import requests

import config
from app.model.audio_challenge import AudioChallenge as AudioChallengeModel
from command.captcha.utils import ACCEPTED_LICENSES, normalize_license

CCMIXTER_URL = "http://ccmixter.org/api/query?f=js&rand=1"

PART_DURATION = 15

FFMPEG_BASE = "ffmpeg -hide_banner -loglevel panic -y "

SLEEP_TIME = 2


class MySounds:
    sounds = []


def get_sound():
    try:
        # If sounds list is empty, let"s refill it
        if len(MySounds.sounds) == 0:
            # No need to spam CCMixter
            time.sleep(SLEEP_TIME)
            # Let"s find some new sounds on CCMixter
            click.echo("C", nl=False)
            new_sounds = requests.get(CCMIXTER_URL, headers=config.USER_AGENT)
            MySounds.sounds = json.loads(new_sounds.text)
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("")
        click.echo("Error while caching new sounds list")
        click.echo("    => %s" % str(e))
        return None

    try:
        # Use sounds one by one
        new_sound = MySounds.sounds.pop()
        # Verify size of media (and estimate time)
        if new_sound["files"][0]["file_rawsize"] < 500000:
            click.echo("S", nl=False)
            return None
        # Verify extension of media
        if new_sound["files"][0]["file_format_info"]["default-ext"] in ("zip",):
            click.echo("X", nl=False)
            return None
        try:
            sound = {
                "file_name": new_sound["files"][0]["file_name"],
                "file_url": new_sound["files"][0]["download_url"],
                "file_ext": new_sound["files"][0]["file_format_info"]["default-ext"],
                "license": normalize_license(new_sound["license_url"].lower()),
                "author": new_sound["user_real_name"],
            }
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo(new_sound["upload_id"])
            click.echo("    => %s" % str(e))
            # And let"s try another media
            return None
        # License should be ok with re-use
        for available_license in ACCEPTED_LICENSES:
            if sound["license"].strip().startswith(available_license):
                click.echo(".", nl=False)
                # Download file in temp folder
                result = requests.get(
                    sound["file_url"], stream=True, headers=config.USER_AGENT
                )
                # TODO: Downloaded source should go somewhere specific for "local" mode
                # And save it on filesystem
                with open("/tmp/%s" % sound["file_name"], "wb") as f:
                    for chunk in result.iter_content(chunk_size=4096):
                        f.write(chunk)
                return sound
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("    => %s" % str(e))
    # No media found
    return None


def get_number_of_active_challenges():
    return AudioChallengeModel.query.filter_by(inactive=False).count()


def get_inactive_challenges():
    return AudioChallengeModel.query.filter_by(inactive=True).all()


def get_all_challenges():
    return AudioChallengeModel.query.all()


def create_challenge(sound):
    try:
        # Each challenge is a part of a sound with some beeps in it
        token = "%064x" % random.getrandbits(256)
        challenge_path = os.path.join(
            config.AUDIO_CHALLENGE_DIR, token[:2], token[2:4], token[4:6]
        )
        os.makedirs(challenge_path)

        # Get duration of orignal sound
        sound_file = "/tmp/%s" % sound["file_name"]
        command = "ffprobe %s" % sound_file
        process = subprocess.Popen(
            command.split(),
            stdout=None,
            stderr=subprocess.PIPE,
        )
        result = b"".join(process.stderr.readlines()).decode("utf-8")
        durations = map(
            int, re.search("([0-9]+:[0-9]+:[0-9]+)", result).group(1).split(":")
        )
        # Compute duration in seconds
        rates = [3600, 60, 1]
        total_duration = sum(
            [rates[k] * duration for k, duration in enumerate(durations)]
        )

        # Get a part of orignal sound
        offset = random.randint(0, total_duration - PART_DURATION)
        part_file = "/tmp/part.ogg"
        # Get a string hour format from offset
        offset_hour = ""
        for rate in (3600, 60):
            if offset > rate:
                offset_hour += "%02d:" % int(offset / rate)
                offset -= int(offset / rate) * rate
            else:
                offset_hour += "00:"
        offset_hour += "%02d" % offset
        command = "%s -i %s -vn -acodec libvorbis -ss %s -t %s %s" % (
            FFMPEG_BASE,
            sound_file.replace("(", "\(").replace(")", "\)"),
            offset_hour,
            PART_DURATION,
            part_file,
        )
        os.system(command)

        # Create some beeps
        beep_file = "/tmp/beep.ogg"
        beep_counter = 0
        previous_beep_type = 0
        duration = 0
        # Total time of beep sound in tenth of second
        total_time = 0
        # Beep type : short or long
        beep_type = None
        # Solution of challenge
        solution = ""
        while total_time < ((PART_DURATION - 2) * 10):
            previous_beep_type = beep_type
            beep_type = random.randint(0, 1)
            frequency = random.randint(80, 160)
            if beep_type == 0:
                solution += "."
                duration = 0.2
            else:
                solution += "-"
                duration = 0.6
            # Creatign beep
            command = (
                '%s -f lavfi -i "sine=frequency=%d0:duration=%s" -af "volume=20dB" %s'
                % (
                    FFMPEG_BASE,
                    frequency,
                    duration,
                    beep_file,
                )
            )
            os.system(command)
            if previous_beep_type is None:
                # First beep
                total_time += random.randint(12, 24)
            elif previous_beep_type == 0:
                # First beep or Previous beep was a short one
                total_time += random.randint(8, 16)
            else:
                # Previous beep was a long one
                total_time += random.randint(12, 20)
            # Prefix beep with a silent
            command = '%s -i %s -af "adelay=%d00" %s' % (
                FFMPEG_BASE,
                beep_file,
                total_time,
                "/tmp/beep_%d.ogg" % beep_counter,
            )
            os.system(command)
            beep_counter += 1

        # Merge original and beeps sounds in the challenge one
        command = "%s -i %s" % (
            FFMPEG_BASE,
            part_file,
        )
        for i in range(beep_counter):
            command += " -i /tmp/beep_%d.ogg" % i
        command += " -filter_complex amix=inputs=%d:duration=longest %s/%s.ogg" % (
            beep_counter + 1,
            challenge_path,
            token,
        )
        os.system(command)

        # Clean part and beeps sounds
        for filename in glob.glob("/tmp/*.ogg"):
            os.remove(filename)

        # Then save challenge in database
        AudioChallengeModel(
            token=token,
            inactive=False,
            solution=solution,
            author=sound["author"],
            license=sound["license"],
        ).save()
        return True
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("    => %s" % str(e))
        if "group" in str(e):
            click.echo("    => %s" % result)
        return False


def delete_sound(sound):
    sound_file = "/tmp/%s" % sound["file_name"]
    os.remove(sound_file)


def delete_challenge(challenge):
    challenge_filepath = challenge.filepath
    challenge_path_1 = os.path.join(
        config.AUDIO_CHALLENGE_DIR, "%s/" % challenge.token[:2]
    )
    challenge_path_2 = os.path.join(challenge_path_1, "%s/" % challenge.token[2:4])
    challenge_path_3 = os.path.join(challenge_path_2, "%s/" % challenge.token[4:6])
    # Deleting challenge
    challenge.delete()
    # Deleting file
    try:
        os.remove(challenge_filepath)
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("    => %s" % str(e))
    # Verifying folders
    if os.listdir(challenge_path_3) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_3)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(challenge_path_2) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_2)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(challenge_path_1) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_1)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
