# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-09-29 22:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: app/controller/user.py:38
msgid "Confirmation password is not identical to password."
msgstr ""

#: app/controller/user.py:40
msgid "This account exists already, pleace create another."
msgstr ""

#: app/controller/user.py:61
msgid "Account disabled."
msgstr ""

#: app/controller/user.py:63
msgid "Bad user or password."
msgstr ""

#: app/controller/user.py:115
#, python-format
msgid ""
"Your new private key is <b>%s</b>. Keep it safely, it won't be re-"
"displayed again!"
msgstr ""

#: app/form/key.py:11 app/view/user/dashboard.html:16
msgid "Domains"
msgstr ""

#: app/form/key.py:13
msgid "Comma separated list of domains for this specific key,"
msgstr ""

#: app/view/base.html:4
msgid "An error has been detected. Please come back later."
msgstr ""

#: app/view/core/about.html:5 app/view/menu.html:10
msgid "About"
msgstr ""

#: app/view/menu.html:13
msgid "Why?"
msgstr ""

#: app/view/menu.html:16
msgid "FAQ"
msgstr ""

#: app/view/menu.html:19
msgid "Demo"
msgstr ""

#: app/view/core/documentation.html:5 app/view/menu.html:22
msgid "Documentation"
msgstr ""

#: app/view/menu.html:30 app/view/user/dashboard.html:7
msgid "Dashboard"
msgstr ""

#: app/view/menu.html:33
msgid "Logout"
msgstr ""

#: app/view/menu.html:37 app/view/user/subscribe.html:5
msgid "Subscribe"
msgstr ""

#: app/view/menu.html:40
msgid "Login"
msgstr ""

#: app/view/core/about.html:7
msgid ""
"Libre Captcha is an application offering a tool in order to block auto-"
"spam."
msgstr ""

#: app/view/core/about.html:9
msgid ""
"This tool is certainly not completely efficient, but it can slow down "
"spambots."
msgstr ""

#: app/view/core/about.html:11
msgid ""
"Libre Captcha is Libre Software (<a "
"href=\"https://www.gnu.org/licenses/agpl-3.0.en.html\">AGPLv3+</a>) and "
"can be freely used."
msgstr ""

#: app/view/core/about.html:13
msgid ""
"Sources are available <a "
"href=\"https://framagit.org/Mindiell/captcha\">here</a>."
msgstr ""

#: app/view/core/about.html:15
msgid ""
"You can contact us by mail at <strong>librecaptcha (at) mytipy (dot) "
"net</strong>."
msgstr ""

#: app/view/core/bot.html:5
msgid "Libre Captcha Bot"
msgstr ""

#: app/view/core/bot.html:9
msgid "This bot is too fast for your website"
msgstr ""

#: app/view/core/bot.html:10
msgid ""
"Actually, this bot should be querying at maximum speed of two seconds. It"
" means that it should not query your website more than once in two "
"seconds, which should be sufficient not to harm your website."
msgstr ""

#: app/view/core/bot.html:12
msgid "This bot should not crawl my website"
msgstr ""

#: app/view/core/bot.html:13
msgid ""
"Actual crawl should be done onto Wikimedia Commons and (CC) Mixter "
"websites. If your website is crawled by this User-Agent maybe someone "
"else is using it."
msgstr ""

#: app/view/core/bot.html:15
msgid "This bot is doing strange things"
msgstr ""

#: app/view/core/bot.html:16
msgid ""
"This bot is still under development and should not harm your website. "
"Please report any abuse or strange behaviors."
msgstr ""

#: app/view/core/developpers.html:5
msgid "Developpers"
msgstr ""

#: app/view/core/developpers.html:7 app/view/core/documentation.html:7
#: app/view/core/why.html:146
msgid "Todo..."
msgstr ""

#: app/view/core/faq.html:5
msgid "Frequently Asked Questions"
msgstr ""

#: app/view/core/faq.html:8
msgid "Why this name?"
msgstr ""

#: app/view/core/faq.html:13
msgid "No logo, really?"
msgstr ""

#: app/view/core/faq.html:14
msgid ""
"As LibreCaptcha is a young project with people without "
"<strong>any</strong> drawing/design talent, there is no logo. As for the "
"name, if you have any idea about a great logo, feel free to send it to "
"us!"
msgstr ""

#: app/view/core/faq.html:18
msgid "Why the website is so ugly?"
msgstr ""

#: app/view/core/faq.html:19
msgid ""
"As stated in previous answer, we don't have <strong>any</strong> "
"drawing/design talent. If you have any skill, feel free to contact us!"
msgstr ""

#: app/view/core/faq.html:23
msgid "Why documentation is not here?"
msgstr ""

#: app/view/core/faq.html:24
msgid ""
"As LibreCaptcha is a young project, it's not done yet but we are working "
"on it!"
msgstr ""

#: app/view/core/faq.html:28
msgid "How can I help?"
msgstr ""

#: app/view/core/faq.html:29
msgid ""
"Translation could be helpful too but not really necessary for now. "
"Website and logo design should be great and are very welcome too. For "
"now, we need help on testing the service, in order to see if it's "
"reliable and user firendly enough for people with deficiencies."
msgstr ""

#: app/view/core/faq.html:33
msgid "How can I contact you?"
msgstr ""

#: app/view/core/faq.html:34
msgid ""
"You can reach us by mail at <strong>librecaptcha (at) mytipy (dot) "
"net</strong>."
msgstr ""

#: app/view/core/home.html:5
msgid "Libre Captcha"
msgstr ""

#: app/view/core/home.html:7
msgid "This application is a test for generating and using CAPTCHAs."
msgstr ""

#: app/view/core/home.html:8
msgid "Currently, there are:"
msgstr ""

#: app/view/core/home.html:15
msgid ""
"In order to use it, you have to subscribe and create a <b>key</b>. Then, "
"you can modify your HTML form by adding a call to a javascript file and "
"putting a specific div inside the form itself like this:"
msgstr ""

#: app/view/core/home.html:17
msgid "Call the script before the form:"
msgstr ""

#: app/view/core/home.html:20
msgid ""
"Add the specific div where you want the Captcha appears with your "
"<b>public key</b>:"
msgstr ""

#: app/view/core/home.html:23
msgid ""
"Server side, you have to verify the user's answer to know what to do next"
" using your <b>private key</b>."
msgstr ""

#: app/view/core/home.html:24
msgid "Et voilà! Nothing more to do, your new LibreCaptcha is now available!"
msgstr ""

#: app/view/core/why.html:5
msgid "The Captcha dilemna"
msgstr ""

#: app/view/core/why.html:7
msgid ""
"Today, on the internet, there are a lot of bots all over our "
"communication tools. They all are spamming us with some kind of ads or "
"links in order to give some websites more visibility. All this spam is "
"consuming lots of resources all over the worls world: programmers who "
"develop them and those who fight against them, electricity, network "
"ressources, memory and storage."
msgstr ""

#: app/view/core/why.html:8
msgid ""
"Some people had ideas about how to fight those bots by using a Turing "
"Test in order to differentiate humans being and computers. The problem is"
" that captchas must be strong enough for computers but easy for humans "
"which is not quite an easy problem. After some time, Google Inc. did some"
" good job at it and let anyone use their captcha to fight against bots. "
"But today, we all know that this tool is no more neutral. It is used by a"
" society to improve its A.I. for free, getting lots of informations about"
" people resolving the captcha and doing it for bad purposes like war and "
"money."
msgstr ""

#: app/view/core/why.html:9
msgid ""
"This is why I did try to get something out of the actual current "
"situation..."
msgstr ""

#: app/view/core/why.html:11
msgid "What is a CAPTCHA for ?"
msgstr ""

#: app/view/core/why.html:12
msgid ""
"A captcha is an acronym for \"Completely Automated Public Turing test to "
"tell Computers and Humans Apart\"."
msgstr ""

#: app/view/core/why.html:13
msgid ""
"It's a test that should be able to detect if the resolver is a Human or a"
" Computer by giving a problem very hard to resolve by a computer but easy"
" to resolve for a human. As the computer evolved the problem evolved too "
"because today there are a bunch of AI able to resolve such tests."
msgstr ""

#: app/view/core/why.html:14
msgid ""
"At first, computer \"vision\" was very poor and they were unable to "
"\"read\" text on images. So first captchas were simply some text on an "
"image. By the time, some programs were then able to read such texts (they"
" are called OCR : Optical Character Recognition), so captchas started to "
"add some \"graphical noise\" around the text, then text was distorted in "
"order to block computer's algorithms."
msgstr ""

#: app/view/core/why.html:15
msgid ""
"Finally, such texts were so much modified that even humans have had "
"difficulties to read them."
msgstr ""

#: app/view/core/why.html:16
msgid ""
"Then comes Google and it's its exciting idea : Captcha can help AI by "
"giving it good answers to a problem the machine can't resolve. So it is, "
"captcha is now used for two distinct goals :"
msgstr ""

#: app/view/core/why.html:18
msgid ""
"Services providers don't want to have bots on their instances consuming "
"ressources"
msgstr ""

#: app/view/core/why.html:19
msgid "Google wants its AI to improve for free"
msgstr ""

#: app/view/core/why.html:22
msgid ""
"At the beginning, it started with some scanned text from books, then it "
"was some street numbers on the street, yesterday it was later on some "
"road signs and cars, and today it's claimed that the AI can detect if you"
" are a human or a bot !"
msgstr ""

#: app/view/core/why.html:23
msgid ""
"It could be all good if it wasn't a way to make users work for free in "
"order to get more money and datas and improve AI for killing and get "
"people killed while masquerading as a good-tech company."
msgstr ""

#: app/view/core/why.html:25
msgid "Important points"
msgstr ""

#: app/view/core/why.html:26
msgid ""
"In order to be able to offer a captcha, there are lot of possibilities. "
"I'm going to discuss a lot of them (if not all, I'm open to new ideas ;o)"
" )."
msgstr ""

#: app/view/core/why.html:28
msgid "Which problem to use ?"
msgstr ""

#: app/view/core/why.html:29
msgid ""
"In order to \"detect\" bots, you can also \"detect\" humans. The problem "
"displayed to users has to be hard for computers and easy for humans. But,"
" as the time passes, computers are more and more effectives and they can "
"resolve lots of problems. Happily there are some subjects where they have"
" more difficulties like vision and understanding."
msgstr ""

#: app/view/core/why.html:31
msgid "Vision problem"
msgstr ""

#: app/view/core/why.html:32
msgid ""
"A vision problem can be of multiple types. Currently  Microsoft, Google "
"and Wikimedia have tried some kind of visual recognition, which is quite "
"a difficult problem for computers."
msgstr ""

#: app/view/core/why.html:33
msgid ""
"Microsoft Asirra Captcha displays some cats and dogs pictures and ask "
"users to pick all the pictures of one of the two categories."
msgstr ""

#: app/view/core/why.html:34
msgid ""
"Google reCaptcha tries to detect if the user is a human, if not it "
"displays some pictures from its projects Google Maps and Google Street "
"View asking users to select only cars or road signs. Again, the visual "
"recognition is done by humans."
msgstr ""

#: app/view/core/why.html:35
msgid ""
"Wikimedia tried some captcha technology by displaying a picture and "
"asking users to select the corresponding tag."
msgstr ""

#: app/view/core/why.html:37 app/view/core/why.html:66
#: app/view/core/why.html:89 app/view/core/why.html:110
msgid "Pros"
msgstr ""

#: app/view/core/why.html:40
msgid "Visual recognition is a very difficult problem for computers"
msgstr ""

#: app/view/core/why.html:41
msgid "Visual recognition is a simple problem for humans"
msgstr ""

#: app/view/core/why.html:42
msgid "Even a child can recognize an animal or a car"
msgstr ""

#: app/view/core/why.html:46 app/view/core/why.html:74
#: app/view/core/why.html:97 app/view/core/why.html:117
msgid "Cons"
msgstr ""

#: app/view/core/why.html:49
msgid ""
"Visual recognition can be a blocking problem for humans with visual "
"deficiencies (blindness, color-blindness, etc...)"
msgstr ""

#: app/view/core/why.html:50
msgid ""
"You got need to get a lot of different pictures in order to be able to "
"have some kind of randomness"
msgstr ""

#: app/view/core/why.html:51
msgid ""
"The computer generating the problem should know the solution, so you need"
" to classify your pictures first"
msgstr ""

#: app/view/core/why.html:52
msgid ""
"Screenreaders can't display such problem (je ne comprends pas ce que ça "
"veut dire mais c'est peut-être normal...)"
msgstr ""

#: app/view/core/why.html:56
msgid "Mathematical or spelling problem"
msgstr ""

#: app/view/core/why.html:57
msgid ""
"A mathematical or spelling problem is a problem often displayed in text "
"only. Its purpose is to detect human by asking them simple questions :"
msgstr ""

#: app/view/core/why.html:59
msgid "Type the last but third letter from the end of the word : \"perspicacity\""
msgstr ""

#: app/view/core/why.html:60
msgid "Guess what is the result of this operation : 1 + 2 ="
msgstr ""

#: app/view/core/why.html:61
msgid "What is the first word of this web site ?"
msgstr ""

#: app/view/core/why.html:62
msgid "etc..."
msgstr ""

#: app/view/core/why.html:69
msgid "Infinite number of mathematical problems"
msgstr ""

#: app/view/core/why.html:70 app/view/core/why.html:93
msgid "Easy generation"
msgstr ""

#: app/view/core/why.html:77
msgid "A lot of translation work is needed"
msgstr ""

#: app/view/core/why.html:78
msgid "What about non occidental alphabets ?"
msgstr ""

#: app/view/core/why.html:79
msgid "Some children can't resolve certain problems"
msgstr ""

#: app/view/core/why.html:80 app/view/core/why.html:101
msgid "Cognitive deficiency can block users (dyslexia, etc...)"
msgstr ""

#: app/view/core/why.html:81
msgid "Often too simple for computers"
msgstr ""

#: app/view/core/why.html:82
msgid "Questions about visited website can't be numerous"
msgstr ""

#: app/view/core/why.html:86
msgid "Reading problem"
msgstr ""

#: app/view/core/why.html:87
msgid ""
"Deformed text can be displayed. The goal of the problem being to read "
"letters and type the same word. By using letters and digits, and not "
"selecting a word, bots can't use dictionary ies for help."
msgstr ""

#: app/view/core/why.html:92
msgid "Infinite number of problems"
msgstr ""

#: app/view/core/why.html:100
msgid "What about non occidental alphabets / keyboards ?"
msgstr ""

#: app/view/core/why.html:102
msgid "Today, too simple for computers"
msgstr ""

#: app/view/core/why.html:103
msgid "Visual deficiency can block users (blindness, visually impaired, etc...)"
msgstr ""

#: app/view/core/why.html:107
msgid "Audio problem"
msgstr ""

#: app/view/core/why.html:108
msgid ""
"The aim of this problem is to have the user listening to a sound and type"
" the word listened."
msgstr ""

#: app/view/core/why.html:113
msgid "Visually impaired people can solve this problem"
msgstr ""

#: app/view/core/why.html:120
msgid "Not easy to generate, or need to have a big collection of native speakers"
msgstr ""

#: app/view/core/why.html:121
msgid "Sound deficiency can block users (deaf, etc...)"
msgstr ""

#: app/view/core/why.html:125
msgid "The problem generation"
msgstr ""

#: app/view/core/why.html:126
msgid ""
"In order to generate a problem, it should be more efficient if a computer"
" can do it. But, if a computer can do it, dos can a computer can solve it"
" ? Well, not necesseralynecessarily."
msgstr ""

#: app/view/core/why.html:127
msgid ""
"The aim of a Captcha is to have a big pool of different problems, so that"
" a bot can't easily store each problem in order to solve them by brute "
"force and remember the solutions. The perfect Captcha should be to never "
"present the same problem twice."
msgstr ""

#: app/view/core/why.html:128
msgid ""
"Plus, the generation of a problem can be tricky because the generation "
"needs to know the solution. There are two possibilities for this :"
msgstr ""

#: app/view/core/why.html:130
msgid "Some humans tells the solution to the computer"
msgstr ""

#: app/view/core/why.html:131
msgid "The computer can create the problem easily but not solve it"
msgstr ""

#: app/view/core/why.html:134
msgid ""
"First possibility is quite impossible, apart if you got a lot of people "
"working days and nights for free. Second possibility can use some "
"algorithms like the one used in cryptography : hash, or more simply a "
"process which gives a result where you can't get back to the beginning."
msgstr ""

#: app/view/core/why.html:136
msgid "Solution(s)"
msgstr ""

#: app/view/core/why.html:137
msgid "Some ideas have come to my mind after such thoughts..."
msgstr ""

#: app/view/core/why.html:139
msgid "Problem generation"
msgstr ""

#: app/view/core/why.html:141
msgid "Visual problem generation"
msgstr ""

#: app/view/core/why.html:142
msgid ""
"Regarding the visual problem generation, a solution could be to take a "
"picture, then cut it up in parts randomly (always the same size, but not "
"the same origin point). With this process, and using N parts of the "
"picture, we are not creating one problem, but N !"
msgstr ""

#: app/view/core/why.html:143
msgid ""
"Another important point is to slightly modify the parts of the pictures "
"in order to block a simple recognition algorithm while a human would be "
"able to solve it easily. The Gaussian Blur filter with a random radius "
"could be ideal for this as you can't easily go back, even more if you "
"don't know the parameters used for it !"
msgstr ""

#: app/view/core/why.html:145
msgid "Audio problem generation"
msgstr ""

#: app/view/core/why.html:148
msgid "Having a big pool of different problems"
msgstr ""

#: app/view/core/why.html:149
msgid ""
"Using Wikimedia Commons can be very efficient. This website has an "
"important stock of medias of different types : "
"https://commons.wikimedia.org/wiki/Special:MediaStatistics"
msgstr ""

#: app/view/core/why.html:150
msgid ""
"Plus, using the special random page "
"(https://commons.wikimedia.org/wiki/Special:Random/File) we can access "
"completely random files without having to download every medium."
msgstr ""

#: app/view/core/why.html:152
msgid "Fair-use and license"
msgstr ""

#: app/view/core/why.html:153
msgid ""
"Medias uploaded on Wikimedia Commons are not all usable without any "
"conditions. Some are under specific license."
msgstr ""

#: app/view/core/why.html:154
msgid ""
"Regarding medias, we can add the Author and License on the complete media"
" but we shouldn't do that on the modified ones ! It can be considered as "
"a fair-use of the media since the media itself will be displayed with all"
" its related informations and the modified one will only be a part of the"
" original one."
msgstr ""

#: app/view/core/why.html:156
msgid "Hotlinking"
msgstr ""

#: app/view/core/why.html:157
msgid ""
"Hotlinking can be interesting if the Captcha ends as a Wikimedia "
"Foundation Project. Reusing existing data is better than copying it all "
"over the internet. But a media could be modified later on and not be "
"linked anymore to the parts."
msgstr ""

#: app/view/core/why.html:158
msgid ""
"Plus, it's mandatory to store the parts of the media to display "
"challenges. So the first solution will be to download and store all the "
"medias used by the Captcha. Once the last part of the media has been "
"used, the media will then be deleted."
msgstr ""

#: app/view/core/why.html:160
msgid "Other idea(s) not choosen"
msgstr ""

#: app/view/core/why.html:162
msgid "Visual test : Little small video game"
msgstr ""

#: app/view/core/why.html:163
msgid ""
"Some modern javascript libraries make it possible to create small video "
"games where the goal is simple."
msgstr ""

#: app/view/core/why.html:164
msgid ""
"It could be some object to pick up and release on a specific place, or "
"just moving something out of a small labyrinth."
msgstr ""

#: app/view/core/why.html:165
msgid ""
"Since this technology is far more complicated and result is bad for "
"disabled users, this idea has been abandonned."
msgstr ""

#: app/view/core/why.html:167
msgid "Offering such a service"
msgstr ""

#: app/view/core/why.html:168
msgid "Sustainability"
msgstr ""

#: app/view/core/why.html:169
msgid ""
"As the service should be sustainable, it has to cost as less little money"
" as possible. In return, the service should be as fast as possible in "
"order not to block users, and simple to use for developers."
msgstr ""

#: app/view/core/why.html:170
msgid ""
"I found such service billed at 7$ by month for unlimited amount of use "
"for 1 website, 20$ for up to 10 websites and 40$ for up to 50 websites. "
"Savings can be done if you buy more than one month in a row."
msgstr ""

#: app/view/core/why.html:171
msgid ""
"Creating some kind of Foundation/Association should be the goal. Joining "
"the Wikimedia Foundation Projects could be a wonderful lever in order to "
"have funds, community, and experimented developers to help."
msgstr ""

#: app/view/core/why.html:173
msgid "Security"
msgstr ""

#: app/view/core/why.html:174
msgid ""
"As the world goes, some people make a business of resolving Captchas, "
"therefore even a strong Captcha is not sufficient to block all bots. But "
"using such a Captcha would, at least, block the text-readers solvers and "
"maybe reduce the size of personal datas that others Captchas (like "
"reCaptcha) are getting from users each minutes."
msgstr ""

#: app/view/core/why.html:176
msgid "A demo ?"
msgstr ""

#: app/view/core/why.html:177
msgid ""
"Finally I made a demo in order to show this solution was feasible and "
"that first step is not so difficult..."
msgstr ""

#: app/view/user/dashboard.html:6
msgid "Delete my account"
msgstr ""

#: app/view/user/dashboard.html:9
msgid "Your keys"
msgstr ""

#: app/view/user/dashboard.html:14
msgid "Public key"
msgstr ""

#: app/view/user/dashboard.html:15
msgid "Secret key"
msgstr ""

#: app/view/user/dashboard.html:17
msgid "Actions"
msgstr ""

#: app/view/user/dashboard.html:27
msgid "Delete this key"
msgstr ""

#: app/view/user/dashboard.html:34 app/view/user/dashboard.html:77
msgid "You still have no key! Create one right now and start using it!"
msgstr ""

#: app/view/user/dashboard.html:42
msgid "Add new key for this domains list :"
msgstr ""

#: app/view/user/dashboard.html:44
msgid "Create new key"
msgstr ""

#: app/view/user/dashboard.html:48
msgid "Your statistics"
msgstr ""

#: app/view/user/dashboard.html:53
msgid "Domain"
msgstr ""

#: app/view/user/dashboard.html:54
msgid "Challenges displayed"
msgstr ""

#: app/view/user/dashboard.html:55
msgid "Challenges failed"
msgstr ""

#: app/view/user/dashboard.html:56
msgid "Challenges resolved"
msgstr ""

#: app/view/user/dashboard.html:57
msgid "Visual challenges"
msgstr ""

#: app/view/user/dashboard.html:58
msgid "Audio challenges"
msgstr ""

#: app/view/user/delete.html:4
msgid "Delete your account"
msgstr ""

#: app/view/user/delete.html:5
msgid ""
"Be very careful, once your account will be deleted, no more challenges "
"could be displayed on your webpages, thus no user would be able to submit"
" any form. You should be sure that you are no more using Libre Captcha on"
" your different websites in order to delete your account without any "
"problem."
msgstr ""

#: app/view/user/delete.html:6
msgid ""
"Plus, once deleted all informations about your domains, keys, and "
"statistics will be completely deleted and won't be recoverable."
msgstr ""

#: app/view/user/delete.html:9
msgid "Ok, I understand and really want to delete my account"
msgstr ""

#: app/view/user/login.html:5
msgid "Log-in"
msgstr ""

#: app/view/user/subscribe.html:13
msgid "Confirm"
msgstr ""

