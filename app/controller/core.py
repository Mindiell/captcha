# encoding: utf-8

from flask import g, render_template

import config
from app.controller.controller import Controller
from app.model.audio_challenge import AudioChallengeModel
from app.model.visual_challenge import VisualChallengeModel


class Core(Controller):
    def home(self):
        g.page = "home"
        g.visual = VisualChallengeModel.query.filter_by(inactive=False).count()
        g.audio = AudioChallengeModel.query.filter_by(inactive=False).count()
        return render_template("core/home.html")

    def about(self):
        g.page = "about"
        return render_template("core/about.html")

    def faq(self):
        g.page = "faq"
        return render_template("core/faq.html")

    def why(self):
        g.page = "why"
        return render_template("core/why.html")

    def documentation(self):
        g.page = "doc"
        return render_template("core/documentation.html")

    def bot(self):
        g.page = "bot"
        return render_template("core/bot.html")
